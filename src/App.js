//import Header from './components/Header'
import ProductList from './components/ProductList';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

//npx json-server --watch db.json --port 8080 
function App(){

    return(
        <div className='container'>
        <Router>
            <Switch>
                <Route exact path="/">
                    <ProductList  />
                </Route>
                <Route path="/add">
                    <AddProduct/>       
                </Route>
                <Route path="/edit/:id">
                    <EditProduct/>
                </Route>
            </Switch>
        </Router>
        </div>

    );
}

export default App
