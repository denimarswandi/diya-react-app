import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

const ProductList = () => {

    const [getProduct, setProduct] = useState([])
    const [title] = useState('List Product')

    const fetchData = async()=>{
        const response = await fetch('http://localhost:8080/products');
        const data = await response.json()
        setProduct(data)
    }

    const deleteProduct = async (id)=>{
        await fetch(`http://localhost:8080/products/${id}`,{
            method:"DELETE",
            headers:{
                'Content-Type':'application/json'
            }
        })
        fetchData()


    }

    useEffect(() => {
        fetchData()
        document.title = title
    }, [title])

    return (
        <div>
            <Link to="/add" className="button is-primary">Add New</Link>
           <table className="table is-striped">
               <thead>
                   <tr>
                   <th>No.</th>
                   <th>Title</th>
                   <th>Price</th>
                   <th>Action</th>
                   </tr>
               </thead>
               <tbody>
                   
                       {
                           getProduct.map((product, index)=>(
                               <tr key={product.id}>
                                   <td>{index + 1}</td>
                                   <td>{product.title}</td>
                                   <td>{product.price}</td>
                                   <td>
                                       <Link to={`/edit/${product.id}`}  className="button is-small is-info">Edit</Link>
                                       <button className="button is-small is-danger" onClick={()=>deleteProduct(product.id)}>Delete</button>
                                   </td>
                               </tr>
                           ))
                       }
                   
               </tbody>
           </table>
        </div>
    )
}

export default ProductList
