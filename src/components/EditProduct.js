import { useState, useEffect } from "react"
import { useParams, useHistory } from "react-router-dom"
const EditProduct = () => {
    const history = useHistory()
    const [title, setTitle] = useState('')
    const [price, setPrice] = useState('')
    const {id} = useParams()

    

    const updateProduct= async (e)=>{
        e.preventDefault();
        let product = {title, price}
        await fetch(`http://localhost:8080/products/${id}`,{
            method:"PUT",
            body:JSON.stringify(product),
            headers:{
                'Content-Type':'application/json'
            }
        });
        history.push('/')
    }
    
    useEffect(() => {
        const getProduct = ()=>{
            fetch(`http://localhost:8080/products/${id}`).then(response=> response.json()).then(
                data =>{
                    setPrice(data.price)
                    setTitle(data.title)
                }
            )
        }
        getProduct()
    }, [id])
    

    return (
        <div>
            <form onSubmit={updateProduct}> 
                <div className="field">
                    <label>Title</label>
                    <div className="control">
                        <input className="input" type="text" value={title} onChange={(e)=> setTitle(e.target.value)}  placeholder="Title" />
                    </div>
                </div>
                <div className="field">
                    <label>Price</label>
                    <div className="control">
                        <input className="input" type="text" value={price} onChange={(e)=> setPrice(e.target.value)} placeholder="Price" />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <button className="button is-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default EditProduct
