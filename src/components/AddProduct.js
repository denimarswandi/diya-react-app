import {useState, useEffect} from 'react'
import { useHistory  } from 'react-router-dom'

const AddProduct = () => {
    const history = useHistory();
    const [getTitle, setTitle] = useState('')
    const [getPrice, setPrice] = useState('')



    const saveProduct= async (e)=>{
        e.preventDefault();
        let product = {title:getTitle, price:getPrice}
        await fetch('http://localhost:8080/products',{
            method:"POST",
            body:JSON.stringify(product),
            headers:{
                'Content-Type':'application/json'
            }
        });
        history.push('/')
    }

    useEffect(()=>{
      document.title = 'Tambah Produk'
    },[])




    return (
        <div className="container" >
            <form onSubmit={saveProduct}>
                <div className="field">
                    <label>Title</label>
                    <div className="control">
                        <input className="input" type="text" value={getTitle} onChange={(e)=> setTitle(e.target.value)}  placeholder="Title" />
                    </div>
                </div>
                <div className="field">
                    <label>Price</label>
                    <div className="control">
                        <input className="input" type="text" value={getPrice} onChange={(e)=> setPrice(e.target.value)} placeholder="Price" />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <button className="button is-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default AddProduct
